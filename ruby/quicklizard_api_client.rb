require 'digest'
require 'net/https'
require 'uri'

module Quicklizard

	module Api

		class Client
			@@_config = {}

			@@_host = 'https://api.quicklizard.com'

			class << self
				
				def configure(config = nil, &block)
					if config.is_a?(Hash)
						@@_config = config
					elsif block_given?					
						block.call(@@_config)
					else
						raise 'Please provide either a configuration hash or a block'
					end
				end

				def config
					@@_config
				end

				def sign_request(path, qs = nil, body = nil)
					raise 'api secret is not configured' and return if config[:api_secret].nil?
					parts = [path]
					now = Time.now.utc.to_i * 1000
					if qs != nil
						qs += '&qts=%s' % now
					elsif body != nil && !body.is_a?(Hash)
						body += '&qts=%s' % now
					elsif qs.nil? && (body.nil? || body.is_a?(Hash))
						qs = 'qts=%s' % now
					end
					_body = body.is_a?(Hash) ? body.to_json : body
					parts << qs unless qs.nil?
					parts << _body unless _body.nil?
					parts << config[:api_secret]
					puts parts.inspect
					digest = Digest::SHA256.hexdigest(parts.join)
					[path, qs, body, digest]
				end

				def get(request_path)
					uri = URI.parse('%s%s' % [@@_host, request_path])
					path, qs, body, digest = sign_request(uri.path, uri.query)
					full_path = [path,qs].compact.join('?')
					http = Net::HTTP.new(uri.host, uri.port)
          if uri.scheme == 'https'
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          end
					request = Net::HTTP::Get.new(full_path)
					request['API_KEY'] = config[:api_key]
					request['API_DIGEST'] = digest
					http.request(request)
				end

				def post(request_path, request_body, json = true)
					uri = URI.parse('%s%s' % [@@_host, request_path])					
					path, qs, body, digest = sign_request(uri.path, uri.query, request_body)
					http = Net::HTTP.new(uri.host, uri.port)
          if uri.scheme == 'https'
					  http.use_ssl = true
					  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          end
          if json
						request = Net::HTTP::Post.new("#{path}?#{qs}", 'Content-Type' => 'application/json')
					else
						request = Net::HTTP::Post.new(path)
					end
					request.body = json ? body.to_json : body
					request['API_KEY'] = config[:api_key]
					request['API_DIGEST'] = digest
					http.request(request)
        end

				def put(request_path, request_body, json = true)
					uri = URI.parse('%s%s' % [@@_host, request_path])					
					path, qs, body, digest = sign_request(uri.path, uri.query, request_body)
					http = Net::HTTP.new(uri.host, uri.port)
          if uri.scheme == 'https'
					  http.use_ssl = true
					  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          end
          if json
						request = Net::HTTP::Put.new("#{path}?#{qs}", 'Content-Type' => 'application/json')
					else
						request = Net::HTTP::Put.new(path)
					end
					request.body = json ? body.to_json : body
					request['API_KEY'] = config[:api_key]
					request['API_DIGEST'] = digest
					http.request(request)
        end

        def download(request_path, local_file)
          File.open(local_file, 'wb') do |f|
            response = get(request_path)
            f.write(response.read_body)
          end
        end

			end

		end

	end

end