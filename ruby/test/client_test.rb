require_relative '../quicklizard_api_client'
require 'json'

Quicklizard::Api::Client.configure do |config|
	config[:api_key] = 'API_KEY'
	config[:api_secret] = 'API_SECRET'
end

response = Quicklizard::Api::Client.get('/api/v2/products')
puts response.body.inspect