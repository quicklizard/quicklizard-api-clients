using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using EEYGeneral;

namespace quicklizard
{
  class Api
  {
    static LogHandler log = new LogHandler();

    private string apiKey = "";
    private string apkSecret = "";
    private static string HostURL = "https://api.quicklizard.com";

    private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

    public Api(string apiKey, string apiSecret)
    {
      this.apiKey = apiKey;
      this.apiSecret = apiSecret;
    }

    public string getRequest(string path, string qs)
    {
      log.WriteLine("***************************************************************");

      string UTCnow = GetCurrentUnixTimestampMillis().ToString();
      string questionOrAmp = qs.indexOf("?") == -1 ? "?" : "&";
      string queryString = qs + questionOrAmp + "qts=" + UTCnow;
      string digest = sha256_hash(path + queryString + this.apiSecret);
      string txtResponse = string.Empty;

      HttpWebRequest request = (HttpWebRequest)WebRequest.Create(RequestURL);
      request.Method = "GET";
      request.Timeout = 300000;
      request.ContentType = "application/json; charset=utf-8";
      request.Headers.Add("API_KEY: " + this.apiKey);
      request.Headers.Add("API_DIGEST: " + digest);

      using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
      using (Stream stream = response.GetResponseStream())
      using (StreamReader reader = new StreamReader(stream))
      {
        txtRespons = reader.ReadToEnd();
      }

      log.WriteLine(txtRespons);
      log.WriteLine("***************************************************************");
      return txtResponse;
    }

    private String sha256_hash(String value)
    {
      using (SHA256 hash = SHA256Managed.Create())
      {
        return String.Join("", hash
          .ComputeHash(Encoding.UTF8.GetBytes(value))
          .Select(item => item.ToString("x2")));
      }
    }

    private long GetCurrentUnixTimestampMillis()
    {
      return (long)(DateTime.UtcNow - UnixEpoch).TotalMilliseconds;
    }

  }
}
