using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

public class ApiClientTest {

  private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

  public static void Main() {
    getReport();
  }

  private static string getSignature(string requestPath, string requestBody = "", string queryString = "") {
    string secret = "API_SECRET";
    string value = requestPath + queryString + requestBody + secret;
    using (SHA256 hash = SHA256Managed.Create())
    {
      return String.Join("", hash
        .ComputeHash(Encoding.UTF8.GetBytes(value))
        .Select(item => item.ToString("x2")));
    }
  }
 
  public static void getReport(){ 
    string key = "API_KEY";
    WebClient client = new WebClient();
    client.Headers.Add("API_KEY", key);
    string requestPath = "/api/v2/products";       
    string queryString = "page=1&qts=" + UnixTimestamp().ToString();

    string digest = getSignature(requestPath, queryString: queryString);     
    client.Headers.Add("API_DIGEST", digest);

    // string newurl = WSWebUtils.JoinUrlParameter(requestHost + requestPath, requestBodyStr);
    byte[] responsebytes = client.DownloadData("https://api.quicklizard.com/api/v2/products?" + queryString);
    string responsebody = Encoding.UTF8.GetString(responsebytes);
    Console.WriteLine(responsebody);
  }

  private static long UnixTimestamp()
  {
    return (long)(DateTime.UtcNow - UnixEpoch).TotalMilliseconds;
  }
}