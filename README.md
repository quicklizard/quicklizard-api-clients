# Quicklizard API Client Libraries

A collection of client libraries to work against the Quicklizard REST API.

The client libraries expose a simple API to perform `GET` and `POST` requests against the Quicklizard API. Additionally
a `download` method is available to perform `GET` requests that download files.

Before starting to use the client, please make sure you call the `configure` method and pass your Quicklizard **API_KEY** and **API_SECRET**.

## Usage

Download / clone this repo and place the relevant client file somewhere in your load path.

### get(request_path)

Perform a `GET` request to a given path identified by `request_path`.

### post(request_path, post_body)

Perform a `POST` request to a given path identified by `request_path` passing a post body identified `post_body`.

### download(request_path, local_file_path)

Perform a `GET` request to a given path identified by `request_path` and write the response to local path identified by `local_file_path`.

## Ruby Client

```ruby
require 'quicklizard_api_client'
require 'json'

Quicklizard::Api::Client.configure do |config|
	config[:api_key] = 'YOUR_API_KEY'
	config[:api_secret] = 'YOUR_API_SECRET'
end

# List all products
products_response = Quicklizard::Api::Client.get('/api/v2/products')
puts products_response.body.inspect

# Create a new download
post_body = 'download[downloader_id]=YOU_API_KEY_HERE&download[args][type]=price_recommendations&download[args][period]=24.hours'
download_response = Quicklizard::Api::Client.post('/api/v2/downloads', post_body)
download = JSON.parse(download_response.body)

# Download a file
Quicklizard::Api::Client.download('/api/v2/downloads/%s/download' % download['id'], 'my_report.xlsx')
```

## PHP Client

```php
require_once('quicklizard_api_client.php');

$qlClient = new QuicklizardApiClient();
$qlClient->configure('API_KEY','API_SECRET');

// list all products
$products = $qlClient->get('/api/v2/products');
echo $products;

// create a new download
$post_body = 'download[downloader_id]=YOU_API_KEY_HERE&download[args][type]=price_recommendations&download[args][period]=24.hours'
$download_response = $qlClient->post('/api/v2/downloads', $post_body);
$download = json_decode($download_response);

// Download a file
$qlClient->download('/api/v2/downloads/'.$download['id'].'/download', 'my_report.xlsx');
```