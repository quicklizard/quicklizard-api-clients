package com.quicklizard.api;

import junit.framework.TestCase;
import us.monoid.web.JSONResource;

import java.io.File;
import java.io.IOException;

public class ClientTest extends TestCase {

  private final String API_KEY = "API_KEY";
  private final String API_SECRET = "API_SECRET";
  private Client apiClient = new Client(API_KEY, API_SECRET);

  public void testClientGET() {
    try {
      JSONResource response = apiClient.get("/api/v2/products?page=1");
      System.out.println("================" + response);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void testClientPOST() {
    try {
      JSONResource response = apiClient.post("/api/v2/downloads", "download[downloader_id]=" + API_KEY + "&download[args][type]=price_recommendations&download[args][period]=1.days");
      System.out.println("================" + response);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void testClientDownload() {
    try {
      File download = apiClient.download("/api/v2/downloads/2217118b-8b76-4b0c-8daa-6dbdc17f09b0/download", "/Users/zohararad/Desktop/50654960-63c5-415c-b8e9-986274037d74.xlsx");
      System.out.println("================" + download);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
