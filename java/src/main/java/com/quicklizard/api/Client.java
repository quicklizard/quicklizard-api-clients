package com.quicklizard.api;

import us.monoid.web.BinaryResource;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;

import java.io.*;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.TimeZone;

public class Client {

  private static String API_HOST = "https://api.quicklizard.com";
  private String key;
  private String secret;

  Client(String apiKey, String apiSecret) {
    this.key = apiKey;
    this.secret = apiSecret;
  }

  public JSONResource get(String path) throws IOException {
    String[] parts = getRequestPathAndQueryString(path);
    String requestPath = parts[0];
    String queryString = parts[1];
    String apiDigest = signRequest(requestPath, queryString, "");
    String apiURL = getRequestURL(requestPath, queryString);
    Resty r = new Resty();
    r.withHeader("accept", "application/json");
    r.withHeader("API_KEY", this.key);
    r.withHeader("API_DIGEST", apiDigest);
    JSONResource result = r.json(apiURL);
    return result;
  }

  public JSONResource post(String path, String body) throws IOException {
    String[] parts = getRequestPathAndQueryString(path);
    String requestPath = parts[0];
    String queryString = parts[1];
    String apiDigest = signRequest(requestPath, queryString, body);
    String apiURL = getRequestURL(requestPath, queryString);
    Resty r = new Resty();
    r.withHeader("accept", "application/json");
    r.withHeader("API_KEY", this.key);
    r.withHeader("API_DIGEST", apiDigest);
    JSONResource result = r.json(apiURL, Resty.form(body));
    return result;
  }

  public File download(String path, String saveAs) throws IOException {
    File f = new File(saveAs);
    if(!f.exists()) f.createNewFile();
    String[] parts = getRequestPathAndQueryString(path);
    String requestPath = parts[0];
    String queryString = parts[1];
    String apiDigest = signRequest(requestPath, queryString, "");
    String apiURL = getRequestURL(requestPath, queryString);
    Resty r = new Resty();
    r.withHeader("accept", "application/json");
    r.withHeader("API_KEY", this.key);
    r.withHeader("API_DIGEST", apiDigest);
    BinaryResource result = r.bytes(apiURL);
    result.save(f);
    return f;
  }

  private String signRequest(String path, String queryString, String body) {
    try{
      String rawDigest = String.format("%s%s%s%s", path, queryString, body, this.secret);
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      byte[] hash = md.digest(rawDigest.getBytes("UTF-8"));
      StringBuffer hexDigest = new StringBuffer();
      for (int i = 0; i < hash.length; i++) {
        String hex = Integer.toHexString(0xff & hash[i]);
        if(hex.length() == 1) hexDigest.append('0');
        hexDigest.append(hex);
      }

      return hexDigest.toString();
    } catch(Exception ex){
      throw new RuntimeException(ex);
    }
  }

  private String getQueryStringWithTs(String queryString) {
    TimeZone tz = TimeZone.getTimeZone("UTC");
    Calendar cal = Calendar.getInstance(tz);
    long ts = cal.getTimeInMillis();
    String qsWithTs = queryString.length() > 0 ? String.format("%s&qts=%d", queryString, ts) : String.format("qts=%d", ts);
    return qsWithTs;
  }

  private String[] getRequestPathAndQueryString(String path) {
    String[] parts = path.split("\\?");
    String[] result = new String[2];
    if(parts.length == 2) {
      result[0] = parts[0];
      result[1] = getQueryStringWithTs(parts[1]);
    } else {
      result[0] = parts[0];
      result[1] = getQueryStringWithTs("");
    }
    return result;
  }

  private String getRequestURL(String requestPath, String queryString) {
    return String.format("%s%s?%s", API_HOST, requestPath, queryString);
  }

}