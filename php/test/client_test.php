<?php

$apiKey = "API_KEY";
$apiSecret = "API_SECRET";

require_once(dirname(__FILE__).'/../quicklizard_api_client.php');

// Example of standard GET request
$qlClient = new QuicklizardApiClient();
$qlClient->configure($apiKey, $apiSecret);
$response = $qlClient->get('/api/v2/products');

print_r($response);

// Example of JSON request
$jsonPayload = '{"products":[{"client_uid":"uid_1","price":12.3, "label": "My Product","meta":{ "cost":11, "inventory":23, "permalink": "http://www.yourstore.com/products/123" },"attrs": [{"name": "brand", "value":"Apple"}]}]}';
$payload = json_decode($jsonPayload);
$response = $qlClient->put('/api/v2/products/bulk_update', $payload);

?>